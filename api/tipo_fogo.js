/*
    Lista apenas os pokemons do tipo fogo (type = fire)
*/

var data = require('../data');

module.exports = function(req, res) {
    var result = data.pokemon.filter((item) => item.type.includes("Fire"));

    //Retorno
    res.json(result)
}