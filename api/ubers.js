/*
    Lista apenas os pokemons com mais de 600 pontos de habilidade (total)
    A lista deve estar ordenada do mais habilidoso para o menor
*/

var data = require('../data')

module.exports = function (req, res) {
    // filtra os pokemons com habilidade total maior que 600
    var result = filtrarPorHabilidadeTotalMinima(data.pokemon, 600)

    // ordena os pokemons do mais habilidoso para o menor
    result = ordenarPorHabilidadeTotalDesc(result)

    //Retorno
    res.json(result)
}

function ordenarPorHabilidadeTotalDesc(pokemon) {
    var resultadoOrdenacao = pokemon.sort(function (a, b) {
        if (a.total < b.total) {
            return 1
        }

        if (a.total > b.total) {
            return -1
        }

        return 0
    })

    return resultadoOrdenacao
}

function filtrarPorHabilidadeTotalMinima(pokemon, pontosHabilidade) {
    var resultadoFiltro = pokemon.filter((poke) => {
        return poke.total > pontosHabilidade
    })

    return resultadoFiltro
}